//----------------------------------------------------//
// Wide Studio Application C++ Source File            //
//            created by Wide Studio source generator //
//----------------------------------------------------//
#ifndef _main_window_H
#define _main_window_H
#include <WScom.h>
#include <WSDappDev.h>

//--- OBJECT includes ---//
#include <WSCmainWindow.h>
#include <WSCmenuArea.h>
#include <WSCpulldownMenu.h>
#include <WSCsform.h>
#include <WSCindexForm.h>
#include <WSCform.h>
#include <WSCtreeList.h>
#include <WSCvbtn.h>
#include <WSClist.h>
#include <WSCtextField.h>
#include <WSCvlabel.h>

//--- OBJECT instance variable ---//
extern WSCmainWindow* main_window;
extern WSCmenuArea* main_menu;
extern WSCpulldownMenu* main_menu_file;
extern WSCsform* main_separator_left;
extern WSCindexForm* index_form_left;
extern WSCform* form_call_graph;
extern WSCtreeList* tree_call_graph;
extern WSCform* form_functions;
extern WSCvbtn* button_function_new;
extern WSCvbtn* button_function_delete;
extern WSClist* list_functions;
extern WSCsform* main_separator_right;
extern WSCtextField* main_textarea;
extern WSCindexForm* index_form_right;
extern WSCform* form_commands;
extern WSCvlabel* label_commands_syntax_error;
extern WSCvbtn* button_fix_syntax_error;
extern WSCvbtn* button_commands_fix_indents;
extern WSClist* list_commands;
extern WSCvbtn* button_commands_ok;
extern WSCform* form_search;
extern WSCform* form_replace;

#endif /* _main_window_H */
