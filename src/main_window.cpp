//----------------------------------------------------//
// MWT C++ Application Source File                    //
//      created by WideStudio Application Builder     //
//----------------------------------------------------//
#include <WScom.h>
#include <WSDappDev.h>

//--- OBJECT includes ---//
#include <WSCmainWindow.h>
#include <WSCmenuArea.h>
#include <WSCpulldownMenu.h>
#include <WSCsform.h>
#include <WSCindexForm.h>
#include <WSCform.h>
#include <WSCtreeList.h>
#include <WSCvbtn.h>
#include <WSClist.h>
#include <WSCtextField.h>
#include <WSCvlabel.h>

//--- OBJECT instance variable ---//
WSCmainWindow* main_window = NULL;
WSCmenuArea* main_menu = NULL;
WSCpulldownMenu* main_menu_file = NULL;
WSCsform* main_separator_left = NULL;
WSCindexForm* index_form_left = NULL;
WSCform* form_call_graph = NULL;
WSCtreeList* tree_call_graph = NULL;
WSCform* form_functions = NULL;
WSCvbtn* button_function_new = NULL;
WSCvbtn* button_function_delete = NULL;
WSClist* list_functions = NULL;
WSCsform* main_separator_right = NULL;
WSCtextField* main_textarea = NULL;
WSCindexForm* index_form_right = NULL;
WSCform* form_commands = NULL;
WSCvlabel* label_commands_syntax_error = NULL;
WSCvbtn* button_fix_syntax_error = NULL;
WSCvbtn* button_commands_fix_indents = NULL;
WSClist* list_commands = NULL;
WSCvbtn* button_commands_ok = NULL;
WSCform* form_search = NULL;
WSCform* form_replace = NULL;

//--- OBJECT src ---//

WSCbase* _create_win_main_window(){


  main_window = new  WSCmainWindow(NULL,"main_window");
      main_window->initialize();
  main_window->setPropertyV(WSNname,"main_window");
  main_window->setPropertyV(WSNtitleString,"WideStudio Test");
  main_window->setPropertyV(WSNx,(short)100);
  main_window->setPropertyV(WSNy,(short)100);
  main_window->setPropertyV(WSNwidth,(unsigned short)1024);
  main_window->setPropertyV(WSNheight,(unsigned short)576);
  main_window->setPropertyV(WSNvis,(WSCbool)1);

  main_menu = new  WSCmenuArea(main_window,"main_menu");
      main_menu->initialize();
  main_menu->setPropertyV(WSNname,"main_menu");
  main_menu->setPropertyV(WSNx,(short)0);
  main_menu->setPropertyV(WSNy,(short)0);
  main_menu->setPropertyV(WSNwidth,(unsigned short)1024);
  main_menu->setPropertyV(WSNheight,(unsigned short)23);
  main_menu->setPropertyV(WSNvis,(WSCbool)1);
  main_menu->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  main_menu->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  main_menu_file = new  WSCpulldownMenu(main_menu,"main_menu_file");
      main_menu_file->initialize();
  main_menu_file->setPropertyV(WSNmenuItems,"Open(O):open:O,SP,Exit(X):exit:X");
  main_menu_file->setPropertyV(WSNlabelString,"File(F)");
  main_menu_file->setPropertyV(WSNname,"main_menu_file");
  main_menu_file->setPropertyV(WSNvis,(WSCbool)1);
  main_menu_file->setPropertyV(WSNwidth,(unsigned short)80);
  main_menu_file->setPropertyV(WSNheight,(unsigned short)21);
    extern void open(WSCbase*);
    main_menu_file->addProcedureV("open","open",open,-1);
    extern void exit(WSCbase*);
    main_menu_file->addProcedureV("exit","exit",exit,-1);

  main_separator_left = new  WSCsform(main_window,"main_separator_left");
      main_separator_left->initialize();
  main_separator_left->setPropertyV(WSNbarValue,"192");
  main_separator_left->setPropertyV(WSNname,"main_separator_left");
  main_separator_left->setPropertyV(WSNx,(short)0);
  main_separator_left->setPropertyV(WSNy,(short)24);
  main_separator_left->setPropertyV(WSNwidth,(unsigned short)1024);
  main_separator_left->setPropertyV(WSNheight,(unsigned short)552);
  main_separator_left->setPropertyV(WSNshadowType,(char)-1);
  main_separator_left->setPropertyV(WSNvis,(WSCbool)1);
  main_separator_left->setPropertyV(WSNanchorTop,(unsigned short)24);
  main_separator_left->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  main_separator_left->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  main_separator_left->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  main_separator_left->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  index_form_left = new  WSCindexForm(main_separator_left,"index_form_left");
      index_form_left->initialize();
  index_form_left->setPropertyV(WSNmenuItems,"Call graph,Functions");
  index_form_left->setPropertyV(WSNname,"index_form_left");
  index_form_left->setPropertyV(WSNx,(short)4);
  index_form_left->setPropertyV(WSNy,(short)4);
  index_form_left->setPropertyV(WSNwidth,(unsigned short)190);
  index_form_left->setPropertyV(WSNheight,(unsigned short)544);
  index_form_left->setPropertyV(WSNvis,(WSCbool)1);
  index_form_left->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  index_form_left->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  index_form_left->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  index_form_left->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  form_call_graph = new  WSCform(index_form_left,"form_call_graph");
      form_call_graph->initialize();
  form_call_graph->setPropertyV(WSNuserValue,(long)1);
  form_call_graph->setPropertyV(WSNname,"form_call_graph");
  form_call_graph->setPropertyV(WSNx,(short)0);
  form_call_graph->setPropertyV(WSNy,(short)24);
  form_call_graph->setPropertyV(WSNwidth,(unsigned short)190);
  form_call_graph->setPropertyV(WSNheight,(unsigned short)520);
  form_call_graph->setPropertyV(WSNshadowType,(char)-1);
  form_call_graph->setPropertyV(WSNvis,(WSCbool)1);
  form_call_graph->setPropertyV(WSNanchorTop,(unsigned short)24);
  form_call_graph->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  form_call_graph->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  form_call_graph->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  form_call_graph->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  tree_call_graph = new  WSCtreeList(form_call_graph,"tree_call_graph");
      tree_call_graph->initialize();
  tree_call_graph->setPropertyV(WSNtitleString,"title1");
  tree_call_graph->setPropertyV(WSNworkHeight,(unsigned short)516);
  tree_call_graph->setPropertyV(WSNname,"tree_call_graph");
  tree_call_graph->setPropertyV(WSNx,(short)0);
  tree_call_graph->setPropertyV(WSNy,(short)0);
  tree_call_graph->setPropertyV(WSNwidth,(unsigned short)190);
  tree_call_graph->setPropertyV(WSNheight,(unsigned short)520);
  tree_call_graph->setPropertyV(WSNvis,(WSCbool)1);
  tree_call_graph->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  tree_call_graph->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  tree_call_graph->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  tree_call_graph->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  form_functions = new  WSCform(index_form_left,"form_functions");
      form_functions->initialize();
  form_functions->setPropertyV(WSNuserValue,(long)2);
  form_functions->setPropertyV(WSNname,"form_functions");
  form_functions->setPropertyV(WSNx,(short)0);
  form_functions->setPropertyV(WSNy,(short)24);
  form_functions->setPropertyV(WSNwidth,(unsigned short)190);
  form_functions->setPropertyV(WSNheight,(unsigned short)520);
  form_functions->setPropertyV(WSNshadowType,(char)-1);
  form_functions->setPropertyV(WSNanchorTop,(unsigned short)24);
  form_functions->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  form_functions->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  form_functions->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  form_functions->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  button_function_new = new  WSCvbtn(form_functions,"button_function_new");
      button_function_new->initialize();
  button_function_new->setPropertyV(WSNlabelString,"New");
  button_function_new->setPropertyV(WSNname,"button_function_new");
  button_function_new->setPropertyV(WSNvis,(WSCbool)1);
  button_function_new->setPropertyV(WSNwidth,(unsigned short)190);
  button_function_new->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  button_function_new->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  button_function_delete = new  WSCvbtn(form_functions,"button_function_delete");
      button_function_delete->initialize();
  button_function_delete->setPropertyV(WSNlabelString,"Delete");
  button_function_delete->setPropertyV(WSNname,"button_function_delete");
  button_function_delete->setPropertyV(WSNvis,(WSCbool)1);
  button_function_delete->setPropertyV(WSNy,(short)30);
  button_function_delete->setPropertyV(WSNwidth,(unsigned short)190);
  button_function_delete->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  button_function_delete->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  list_functions = new  WSClist(form_functions,"list_functions");
      list_functions->initialize();
  list_functions->setPropertyV(WSNtitleString,"title4");
  list_functions->setPropertyV(WSNworkHeight,(unsigned short)456);
  list_functions->setPropertyV(WSNname,"list_functions");
  list_functions->setPropertyV(WSNx,(short)0);
  list_functions->setPropertyV(WSNy,(short)60);
  list_functions->setPropertyV(WSNwidth,(unsigned short)190);
  list_functions->setPropertyV(WSNheight,(unsigned short)460);
  list_functions->setPropertyV(WSNvis,(WSCbool)1);
  list_functions->setPropertyV(WSNanchorTop,(unsigned short)60);
  list_functions->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  list_functions->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  list_functions->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  list_functions->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  main_separator_right = new  WSCsform(main_separator_left,"main_separator_right");
      main_separator_right->initialize();
  main_separator_right->setPropertyV(WSNbarValue,"544");
  main_separator_right->setPropertyV(WSNname,"main_separator_right");
  main_separator_right->setPropertyV(WSNx,(short)198);
  main_separator_right->setPropertyV(WSNy,(short)4);
  main_separator_right->setPropertyV(WSNwidth,(unsigned short)822);
  main_separator_right->setPropertyV(WSNheight,(unsigned short)544);
  main_separator_right->setPropertyV(WSNshadowType,(char)-1);
  main_separator_right->setPropertyV(WSNvis,(WSCbool)1);
  main_separator_right->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  main_separator_right->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  main_separator_right->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  main_separator_right->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  main_textarea = new  WSCtextField(main_separator_right,"main_textarea");
      main_textarea->initialize();
  main_textarea->setPropertyV(WSNmargin,(unsigned char)3);
  main_textarea->setPropertyV(WSNfont,(unsigned char)2);
  main_textarea->setPropertyV(WSNname,"main_textarea");
  main_textarea->setPropertyV(WSNx,(short)4);
  main_textarea->setPropertyV(WSNy,(short)4);
  main_textarea->setPropertyV(WSNwidth,(unsigned short)542);
  main_textarea->setPropertyV(WSNheight,(unsigned short)536);
  main_textarea->setPropertyV(WSNvis,(WSCbool)1);
  main_textarea->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  main_textarea->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  main_textarea->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  main_textarea->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  index_form_right = new  WSCindexForm(main_separator_right,"index_form_right");
      index_form_right->initialize();
  index_form_right->setPropertyV(WSNmenuItems,"Commands,Search,Replace");
  index_form_right->setPropertyV(WSNname,"index_form_right");
  index_form_right->setPropertyV(WSNx,(short)550);
  index_form_right->setPropertyV(WSNy,(short)4);
  index_form_right->setPropertyV(WSNwidth,(unsigned short)268);
  index_form_right->setPropertyV(WSNheight,(unsigned short)536);
  index_form_right->setPropertyV(WSNvis,(WSCbool)1);

  form_commands = new  WSCform(index_form_right,"form_commands");
      form_commands->initialize();
  form_commands->setPropertyV(WSNuserValue,(long)1);
  form_commands->setPropertyV(WSNname,"form_commands");
  form_commands->setPropertyV(WSNx,(short)0);
  form_commands->setPropertyV(WSNy,(short)24);
  form_commands->setPropertyV(WSNwidth,(unsigned short)268);
  form_commands->setPropertyV(WSNheight,(unsigned short)512);
  form_commands->setPropertyV(WSNshadowType,(char)-1);
  form_commands->setPropertyV(WSNvis,(WSCbool)1);
  form_commands->setPropertyV(WSNanchorTop,(unsigned short)24);
  form_commands->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  form_commands->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  form_commands->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  form_commands->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  label_commands_syntax_error = new  WSCvlabel(form_commands,"label_commands_syntax_error");
      label_commands_syntax_error->initialize();
  label_commands_syntax_error->setPropertyV(WSNname,"label_commands_syntax_error");
  label_commands_syntax_error->setPropertyV(WSNvis,(WSCbool)1);
  label_commands_syntax_error->setPropertyV(WSNwidth,(unsigned short)268);
  label_commands_syntax_error->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  label_commands_syntax_error->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  button_fix_syntax_error = new  WSCvbtn(form_commands,"button_fix_syntax_error");
      button_fix_syntax_error->initialize();
  button_fix_syntax_error->setPropertyV(WSNlabelString,"Fix syntax error");
  button_fix_syntax_error->setPropertyV(WSNname,"button_fix_syntax_error");
  button_fix_syntax_error->setPropertyV(WSNvis,(WSCbool)1);
  button_fix_syntax_error->setPropertyV(WSNy,(short)30);
  button_fix_syntax_error->setPropertyV(WSNwidth,(unsigned short)268);
  button_fix_syntax_error->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  button_fix_syntax_error->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  button_commands_fix_indents = new  WSCvbtn(form_commands,"button_commands_fix_indents");
      button_commands_fix_indents->initialize();
  button_commands_fix_indents->setPropertyV(WSNlabelString,"Fix indents");
  button_commands_fix_indents->setPropertyV(WSNname,"button_commands_fix_indents");
  button_commands_fix_indents->setPropertyV(WSNvis,(WSCbool)1);
  button_commands_fix_indents->setPropertyV(WSNy,(short)60);
  button_commands_fix_indents->setPropertyV(WSNwidth,(unsigned short)268);
  button_commands_fix_indents->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  button_commands_fix_indents->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  list_commands = new  WSClist(form_commands,"list_commands");
      list_commands->initialize();
  list_commands->setPropertyV(WSNtitleString,"title1");
  list_commands->setPropertyV(WSNworkHeight,(unsigned short)388);
  list_commands->setPropertyV(WSNname,"list_commands");
  list_commands->setPropertyV(WSNx,(short)0);
  list_commands->setPropertyV(WSNy,(short)90);
  list_commands->setPropertyV(WSNwidth,(unsigned short)268);
  list_commands->setPropertyV(WSNheight,(unsigned short)392);
  list_commands->setPropertyV(WSNvis,(WSCbool)1);
  list_commands->setPropertyV(WSNanchorTop,(unsigned short)90);
  list_commands->setPropertyV(WSNanchorBottom,(unsigned short)30);
  list_commands->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  list_commands->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  list_commands->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  list_commands->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  button_commands_ok = new  WSCvbtn(form_commands,"button_commands_ok");
      button_commands_ok->initialize();
  button_commands_ok->setPropertyV(WSNlabelString,"OK");
  button_commands_ok->setPropertyV(WSNname,"button_commands_ok");
  button_commands_ok->setPropertyV(WSNvis,(WSCbool)1);
  button_commands_ok->setPropertyV(WSNy,(short)482);
  button_commands_ok->setPropertyV(WSNwidth,(unsigned short)268);
  button_commands_ok->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  button_commands_ok->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  button_commands_ok->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  form_search = new  WSCform(index_form_right,"form_search");
      form_search->initialize();
  form_search->setPropertyV(WSNuserValue,(long)2);
  form_search->setPropertyV(WSNname,"form_search");
  form_search->setPropertyV(WSNx,(short)0);
  form_search->setPropertyV(WSNy,(short)24);
  form_search->setPropertyV(WSNwidth,(unsigned short)268);
  form_search->setPropertyV(WSNheight,(unsigned short)512);
  form_search->setPropertyV(WSNshadowType,(char)-1);
  form_search->setPropertyV(WSNanchorTop,(unsigned short)24);
  form_search->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  form_search->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  form_search->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  form_search->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

  form_replace = new  WSCform(index_form_right,"form_replace");
      form_replace->initialize();
  form_replace->setPropertyV(WSNuserValue,(long)3);
  form_replace->setPropertyV(WSNname,"form_replace");
  form_replace->setPropertyV(WSNx,(short)0);
  form_replace->setPropertyV(WSNy,(short)24);
  form_replace->setPropertyV(WSNwidth,(unsigned short)268);
  form_replace->setPropertyV(WSNheight,(unsigned short)512);
  form_replace->setPropertyV(WSNshadowType,(char)-1);
  form_replace->setPropertyV(WSNanchorTop,(unsigned short)24);
  form_replace->setPropertyV(WSNanchorTopFlag,(WSCbool)1);
  form_replace->setPropertyV(WSNanchorBottomFlag,(WSCbool)1);
  form_replace->setPropertyV(WSNanchorLeftFlag,(WSCbool)1);
  form_replace->setPropertyV(WSNanchorRightFlag,(WSCbool)1);

   main_window->setVisible(True);
   return main_window;
}

//--- end of src ---//
